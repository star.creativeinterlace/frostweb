import React from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import columns from './columnsDef';
const fp = require('lodash/fp');

const SimpleTable = (details = []) => {
  const rows = fp.get('details')(details);

  const renderColumnHeaders = () => {
    return (
      <TableRow>
        {columns.map(head => (
          <TableCell key={head.key}>{head.name}</TableCell>
        ))}
      </TableRow>
    );
  }

  const renderCell = row =>
    columns.map((head, i) => (
      <TableCell>{fp.get(head.key)(row)}</TableCell>
    ));
  
  const renderRows = () =>
    rows.map(row => (
      <TableRow
        hover
      >
        {renderCell(row)}
      </TableRow>
    ));

  return (
    <Paper style={{ marginTop: '20px', width: '600px', padding: '10px' }}>
      <Table>
        <TableHead>{renderColumnHeaders()}</TableHead>
        <TableBody>{renderRows()}</TableBody>
      </Table>
    </Paper>
  );
}

SimpleTable.propTypes = {
  details: PropTypes.arrayOf(PropTypes.shape({})).isRequired
};

export default SimpleTable;