import React from 'react';
import logo from './logo.svg';
import './App.css';

import WeatherStation from '../../components/WeatherStation';

const App = () => (
  <div className="App">
    {/* <header className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
    </header> */}
    <WeatherStation />
  </div>
);

export default App;
